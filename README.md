# CI Jekyll

This project is used to demo continuous integration, delivery, deployment using Jekyll.

Tests include HTML-Proofer and Hunspell.

The site is staged on GitLab Pages and manually deployed to EdgeCast Networks.

Fork this project to try it yourself (You will not be able to deploy to the EdgeCast CDN w/o an account and defining the secrets in variables).
