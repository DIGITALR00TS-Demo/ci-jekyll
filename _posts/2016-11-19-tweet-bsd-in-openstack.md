---
title: "Post: Tweet FreeBSD w/ Cloud-Init in OpenStack"
categories:
  - Media
tags:
  - content
  - embeds
  - media
  - twitter
---

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">I have FreeBSD w/ Cloud-Init in OpenStack! <a href="https://t.co/xDtcnXuHno">https://t.co/xDtcnXuHno</a> Thx for the walk through <a href="https://twitter.com/bsdnow">@BSDNow</a></p>&mdash; DIGITALR00TS (@DIGITALR00TS) <a href="https://twitter.com/DIGITALR00TS/status/800187583078625281">November 20, 2016</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

This post tests Twitter Embeds.
